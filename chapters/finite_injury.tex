\chapter{Finite Injury Priority Method}
Finite injury priority method is used to construct a desired problem, i.e. set that have the property we want. In this method, the set is constructed \emph{by stages} to meet certain requirements $\{R_n\}_{n \in \N}$. ``Priority'' means we will assign different priority to each requirement. Requirement $R_n$ has higher priority than requirement $R_m$ with $n < m$, i.e. we will try to satisfy $R_n$ earlier than $R_m$ with $n < m$. Action taken for satisfying $R_m$ at stage $t$ may be undone later at stage $s > t$ when we are satisfying $R_n$ with $n < m$, even if $R_m$ is already satisfied, which is called that $R_m$ is \emph{injured} at stage $s$. That is where ``injury'' comes. Finally, ``finite'' comes from the property of this method that a requirement is injured at most finite times\cite[110]{soare87}. 

In this chapter, I will introduce finite injury priority method using proofs of two interesting and inspiring theorems as examples.
\section{Friedberg-Muchnik Theorem}
In his fundamental paper introducing Turing degrees in recursion theory\cite[77]{soare87}, Post attempted to classify the r.e. sets and r.e. degrees\cite[291]{post44}, therefore raised the question which later became known as ``Post's Problem'': are there more than two r.e. degrees? Friedberg and Muchnik gave a positive answer to this problem independently, using finite injury priority method. Following are the theorem they proved and a concise proof of mine adapted from Soare's book\cite[118]{soare87}:
\begin{theorem}[Friedberg-Muchnik]
There exists two r.e. sets $\A$ and $\B$ such that $\A \not\leq_T \B$ and $\B \not \leq_T \A$ (incomparable).
\end{theorem}

\begin{proof}
We can construct these two sets by the so-called ``Finite Injury Priority Method.''

Generally speaking, we have the following requirements to meet, in order to make $\A$ and $\B$ incomparable:
\begin{equation} R_{2e}: \A \neq \phi_e^\B \end{equation}
\begin{equation} R_{2e+1}: \B \neq \phi_e^\A, \end{equation}
where $\phi_e^\A$ is the partial recursive function computed by the $e$-th relativised Turing Machine using oracle $\A$.

Let $\A = \cup_{s \in \N} \A_s$, $\B = \cup_{s \in \N} \B_s$ , where $\A_s$ and $\B_s$ be the sets we obtain at stage $s$ for $\A$ and $\B$, respectively. Let disagreement witnesses $x_e = \lim_{s \rightarrow \infty} x_{e,s}$, where $x_{e,s}$ are potential witnesses for the following detailed requirement $R_{e,s}$ at each stage $s$:
\begin{equation} R_{2e, s}: M_{e,s}^{\B_s}(x_{e,s}) \downarrow = 0 \end{equation}
\begin{equation} R_{2e+1, s}: M_{e,s}^{\A_s}(x_{e,s}) \downarrow = 0, \end{equation}
where $M_{e,s}^\A$ is the $e$-th relativised Turing Machine using oracle $\A$, in the restriction of  $s$ steps of computation. In order to limit injuries, we will also calculate a restraint number $r_{e,s}$ in our construction of $\A$ and $\B$. 

The whole procedure is as follows:

\paragraph{Stage $0$:}
Set $\A_0 = \B_0 = \varnothing$, and $\forall e$, set $x_{e,0} = \la 0,e \ra$ (where $\la x, y \ra$ is the standard pairing function, in order to make witnesses for different requirements different), $ r_{e,0} = -1$ (meaning they need to be checked).
\paragraph{Stage $s+1$:}
Let $i =  min\{ e \vert R_{e,s} \mbox{ is satisfied with } r_{e,s} = -1 \}$.
\begin{itemize}
	\item \emph{$i = 2e$}: add $i$ into $\A$ by setting $\A_{s+1} = \A_s \cup \{ i\}$, keep $\B_{s+1} = \B_s$. Define the use function $u$ as 
	\begin{equation} u(\A;e,s,x) = \max\{y| M_{e,s}^{\A}(x) \mbox{ ask the oracle `} y \in \A\mbox{?'}\}. \end{equation} Calculate $r_{i,s} = u(\B_s;i,s,x_{i,s}).$
	\begin{itemize}
		\item $\forall j < i$: set $r_{j,s+1} = r_{j,s}$, $x_{j,s+1} = x_{j,s}$.
		\item $\forall j > i$: set $r_{j,s+1} = -1$, and \begin{align}
		x_{j,s+1} = min \{  x \vert & x \not\in \A_{s+1} \cup \B_{s+1} \mbox{ and } x = \la y,j \ra \notag \\                      & \mbox{ and } y > r_{i',s} , \forall i' \le i \mbox{ and } x \ge x_{j,s'}, \forall s' \leq s \} .
		\end{align}
	\end{itemize}
	\item \emph{$i = 2e+1$}: exactly the same as above, except that all things about $\B$ should be replaced by $\A$, and $\A$ replaced by $\B$.
	\item \emph{No such $i$ exists}: set $\A_{s+1} = \A_s$, $\B_{s+1} = \B_s$, $\forall e$, $x_{e,s+1} = x_{e,s}$, $r_{e,s+1} = r_{e,s}$, and move on.
\end{itemize}

\begin{lemma}
  By the above construction, $\A$ and $\B$ are recursively enumerable sets.
\end{lemma}
\begin{proof}
Note that we only add numbers into $\A$ and $\B$, hence it is sufficient to prove each stage is indeed recursive in order to prove $\A$ and $\B$ r.e. However, it is not easy to observe their recursiveness, since the above description is more likely in a mathematician's manner, involving setting infinite ones of $x_{e,s}$ and $r_{e,s}$. Nonetheless, from a programming perspective, we don't have to update all them, which makes each stage recursive. Observing that $M_{e,s'}^{\A_s}(x_{e,s}) \downarrow = M_{e,s}^{\A_s}(x_{e,s}), \forall s' \geq s $ whenever $M_{e,s}^{\A_s}(x_{e,s}) \downarrow$, it is enough for our construction to consider satisfied $R_{e,s}$ with $e \le s $ at each stage $s$. Therefore at each stage we only have a limited number of requirements to check, thus a limited number of $x_{e,s}$ and $r_{e,s}$ to update, by maintaining a list of `need-to-check' tuples of $\la e, s, x_{e,s}, r_{e,s} \ra$ technically.
\end{proof}

\begin{lemma}
All the requirements are satisfied.
\end{lemma}
\begin{proof}
Let $r_e = \lim_{s \rightarrow \infty} r_{e,s}$. Observe that $r_{e,s}$ will only be reset to $-1$ by requirements $R_{e',s}$ with $e' \leq e$, which makes it `injured' at most finite times. (Hence this method is called ``Finite Injury Method'') Therefore, for each $e$, if there is an $x $ such that $M_e^\B (x) \downarrow = 0$, there exists $s_e$ such that $r_e = r_{e, s_e}$ and $x_e = x_{e,s_e}$. Then we have, e.g.
\begin{equation} M_{e,s_e}^\B (x_e) \simeq M_{e,s_e}^{\B \upharpoonright r_e}(x_e) \simeq M_{e,s_e}^{\B_{s_e}}(x_{s_e}) \downarrow = 0 \neq A_{s_e}(x_{e,s_e}) = A (x_e), \end{equation}
by the definition of $r_{e,s}$ (for the first equality), and since we will prevent numbers smaller that $r_e$ entering $\B$ after stage $s_e$ (for the second equality). Therefore $x_e$ is really a disagreement witness for the original requirements that we need to care about. (Notice that some requirements $R_{2e}$ or $R_{2e+1}$ are satisfied automatically if there is no $x$ such that $M_e^\B (x) \downarrow = 0$, for $R_{2e}$ e.g.) 
\end{proof}

Therefore, we get two r.e. sets $\A$ and $\B$ that are incomparable.

\end{proof}

\section{Splitting Theorem}
Finite injury priority method can also be used to proved the following theorem, which was first proved by Sacks, generalizing two former results of Friedberg\cite[121]{soare87}. Particularly, the technique in this proof is very powerful and is essential on infinite injury priority method, which is a further development of finite injury priority method\cite[129]{soare87}. Although we don't need infinite injury to prove the main theorem in this thesis, it remains a good and useful exercise to prove this theorem to get familiar with finite injury priority method.
\begin{theorem}
For any non-recursive r.e. set $\C$, there exists two r.e. sets $\D_0$ and $\D_1$ such that $\D_0$ and $\D_1$ are incomparable, $\D_0 \cup \D_1 = \C$, $\D_0 \cap \D_1 = \varnothing$.
\end{theorem}
\begin{proof} 
In this proof, we will construct $\D_0$ and $\D_1$ such that $\C \not\leq_T \D_0$, $\C \not\leq_T \D_1$, and that $\C$ is the disjoint union of $\D_0$ and $\D_1$. Thus we have the following negative requirements to satisfy:
\begin{equation} N_{\la e, i\ra}:  C \neq \phi_e^{\D_i}, i = 0, 1.\end{equation}

$\C$ is non-recursive r.e., hence there is a recursive enumeration without repetition $g$ of $\C$, i.e. $\C = \{g(0), g(1), g(2), \ldots\}$. Denote $\C_s = \{g(0), \ldots, g(s)\}$. The positive requirements are the following:
\begin{equation} P_s: g(s) \in \D_0 \vee g(s) \in \D_1. \end{equation}

Let $\D_0 = \cup_{s \in \N} \D_{0,s}$, $\D_1 = \cup_{s \in \N} \D_{1,s}$ , where $\D_{0,s}$ and $\D_{1,s}$ be the sets we obtain at stage $s$ for $\D_0$ and $\D_1$, respectively. The whole procedure is as follow:

\paragraph{Stage $0$:}
Set $\D_{0,0} = \varnothing$, $\D_{1,0} = \varnothing$.
\paragraph{Stage $s+1$:}
Calculate the following two functions:
\begin{align}
\mbox{length function: } & l(i,e,s) = \{x | \forall y < x, M_{e,s}^{\D_{i,s}}(y)\downarrow = \C_s(y)\}  \\
\mbox{restraint function: } & r(i,e,s) = \max\{u(\D_{i,s};e,s,y) | y \leq l(i,e,s)\}.
\end{align}
Notice that we use ``$y \leq l(i,e,s)$'' in our definition of $r(i,e,s)$, hence for each requirements we will obtain $\{y | y < l(i,e,s) \}$ as \emph{agreement} witnesses and $y = l(i,e,s)$ as a potential \emph{disagreement} witness if possible. 

Let $\la e,i \ra = \min\{\la e', i'\ra | g(s) \leq r(i',e',s)\}$, which means we will choose the negative requirement with the highest priority that will be injured by enumerating $g(s)$ in \emph{its} side, and we say that all the $\la e', i' \ra$'s with $g(s) \leq r(i',e',s)$ \emph{receive attentions} at stage $s$.
\begin{itemize}
  \item \emph{No such $\la e,i \ra$ exists}: Let $\D_{0,s+1} = \D_{0,s} \cup \{g(s)\}$, $\D_{1,s+1} = \D_{1,s}$, since we will not injure anything.
  \item \emph{Otherwise}: Let $\D_{1-i,s+1} = \D_{1-i, s} \cup \{g(s)\}$, $\D_{i,s+1} = \D_{i,s}$, which means we will enumerate $g(s)$ in the \emph{other} side.
\end{itemize}

\begin{lemma}
The constructed sets $\D_0$ and $\D_1$ are recursively enumerable.
\end{lemma}
\begin{proof}
Similarly as the proof of recursive enumerability of $\A$ and $\B$ in Friedberg-Muchnik Theorem.
\end{proof}

\begin{lemma}
The following three identities hold for all $\la e,i \ra$:
\begin{enumerate}[(i)]
  \item \label{first} The injury set $I_{e,i} = \{x | \exists s, x \leq r(i,e,s) \mbox{ and } x \in \D_i - \D_{i,s} \}$ is finite.
  \item \label{second} $\C \neq \phi_e^{\D_i}$.
  \item \label{third} $r(i,e) = \lim_{s \rightarrow \infty} r(i,e,s)$ exists and is finite.
\end{enumerate}
\end{lemma}

\begin{proof}
We will prove them by induction on $\la e, i \ra$.
To use induction for a $\la e,i \ra$ we want to prove them on, first assume that the above three identities hold for all $\la e', i'\ra < \la e,i \ra$. Define $ss(i',e') = \min\{s| \forall s' \geq s, r(i',e',s') = r(i',e')\}$ for all $\la e',i' \ra < \la e,i \ra$. Let $r_{\max} = \max\{r(i',e') | \la e',i'\ra < \la e, i \ra\} + 1$, $s_{\max} = \max\{ss(i',e') | \la e',i' \ra < \la e,i \ra\}$. Since $r_{\max}$ is finite, there exist some stage $s$ s.t. $\C_s \upharpoonright r_{\max}= \C \upharpoonright r_{\max}$, fix such a $s$ with $s \geq s_{\max}$ for the following proof.

For \ref{first}, after stage $s$, there will not be numbers smaller than $r_{\max}$ entering $\D_0$ and $\D_1$, and $r(i',e',s)$ with $\la e', i'\ra < \la e,i \ra$ will not change any more, hence none of the $\la e',i'\ra$'s with $\la e',i'\ra < \la e,i \ra$ will receive attentions. That is to say, for stage $s' \geq s$, if receiving attentions $\la e,i \ra$ will be with the highest priority, thus by our construction $g(s')$ will be enumerated into the other side $\D_{1-i}$ instead of $\D_i$. Therefore $\la e,i \ra$ will not be injured from stage $s$ on, i.e. the injury set is finite.

Before proving the remaining two parts, we have some useful observations: for any stage $s' \geq s$, 
\begin{enumerate}
  \item \label{ob:1}for $y < l(i,e,s')$, since $\la e,i\ra$ will not be hurt, $M_{e,s'}^{\D_{i,s'+1}}(y) \simeq M_{e,s'}^{\D_{i,s'}}(y) \downarrow $. Hence $M_{e,s'+1}^{\D_{i,s'+1}}(y) = M_{e,s'}^{\D_{i,s'}}(y) = C_{s'}(y)$, $u(\D_{i,s'+1};e,s'+1,y) = u(\D_{i,s'};e,s',y)$.
  \item \label{ob:2}for $y = l(i,e,s')$, if $M_{e,s'}^{\D_{i,s'}}(y) \downarrow$, then $M_{e,s'}^{\D_{i,s'}}(y) \neq C_{s'}(y)$. Moreover, we have $M_{e,s'+1}^{\D_{i,s'+1}}(y) = M_{e,s'}^{\D_{i,s'}}(y) \neq C_{s'}(y)$ and $u(\D_{i,s'+1};e,s'+1,y) = u(\D_{i,s'};e,s',y)$ similarly as above.
\end{enumerate}

For \ref{second}, suppose not then we can prove $\C$ is recursive by showing a procedure to compute $\C$ as follows, which leads to a contradiction: for any $q \in \N$, since $\lim_{s \rightarrow \infty} l(i,e,s) = \infty$ we wait until some stage $s' \geq s$ such that $l(i,e,s') > q$, output $M_{e,s'}^{\D_{i,s'}}(q)$. It remains to prove inductively that 
\begin{equation} \forall t \geq s'~(l(i,e,t) > q \mbox{ and }  r(i,e,t) \geq \max\{u(\D_{i,s'};e,s',y)| y \leq q \}), \end{equation}
 thus using observation \ref{ob:1}, by induction $\forall t \geq s'~(M_e^{\D_{i,t}}(q) = M_e^{\D_{i,s'}}(q))$, therefore $M_{e,s'}^{\D_{i,s'}}(q) \downarrow = M_e^{\D_{i,s'}}(q) = M_e^{\D_i}(q) = \C(q)$, i.e. we have correctly computed $\C(q)$ by outputting $M_{e,s'}^{\D_{i,s'}}(q)$. 
\begin{itemize}
  \item To prove it, assume it holds for $t$. Then for $t+1$, by observation \ref{ob:1}, the computation for $y \leq q$ remains the same, hence $l(i,e,t+1) > q$ and so $r(i,e,t+1) \geq \max\{u(\D_{i,t+1};e,t+1,y)| y \leq q \} = \max\{u(\D_{i,s'};e,s',y)| y \leq q \}$, unless there is some $y \leq q$ s.t. $\C_{t}(y) \neq \C_{t+1}(y)$. 
  \begin{itemize}
      \item If so, denote the least one as $q'$, thus $l(i,e,t+1) = q'$. Since we only add numbers into $\C$, it must be the situation that $\C_{t}(q') = 0$ and $\forall t' > t, \C_{t'}(q') = 1$. By observation $\ref{ob:2}$, using induction we can prove for all $t' > t$, $M_{e,t'}^{\D_{i,t'}}(q') \simeq M_{e,t}^{\D_{i,t}}(q')\downarrow = \C_t(q') \neq \C_{t'}(q') = \C(q')$, hence $M_e^{\D_i}(q') \neq \C(q')$, a contradiction to our assumption.
  \end{itemize}
\end{itemize}

By \ref{second} we can obtain $p = \mu y(M_e^{\D_i}(y) \not\simeq \C(y))$ for proving \ref{third}. Select a sufficient large $s' \geq s$ such that for all $s'' \geq s'$,
\begin{enumerate}[(a)]
\item $\forall y < p, M_{e,s''}^{\D_{i,s''}}(y) \downarrow = M_e^{\D_i}(y)$,
\item $\forall y \leq p, \C_{s''}(y) = \C(y)$, 
\end{enumerate}
i.e. $s'$ is large enough that $M_{e,s'}^{\D_{i,s'}}$ and $\C_{s'}$ will not change any more for $y < p$ and $y \leq p$, respectively. Then there are two possibilities:
\begin{itemize}
  \item $M_e^{\D_i}(p) \uparrow$: then $\forall s'' \geq s'$, $l(i,e,s'') = p$, so using observation \ref{ob:1} by induction, $r(i,e,s'') = r(i,e,s')$. Hence $r(i,e) = r(i,e,s')$ is finite.
  \item $\exists t \geq s', M_{e,t}^{\D_{i,t}}(p) \downarrow \neq \C(p)$: then using observation \ref{ob:2} by induction, we can prove $$ \forall t' \geq t~(M_{e,t'}^{\D_{i,t'}}(p) \downarrow \neq \C(p) \mbox{ and } l(i,e,t') = p \mbox{ and } r(i,e,t') = r(i,e,t)),$$ thus $r(i,e) = r(i,e,t)$ is finite.
\end{itemize}
\end{proof}

Therefore, by the construction we have $\D_0, \D_1 <_T \C$. If $\D_0$ and $\D_1$ are comparable, say e.g., $\D_0 \leq_T \D_1$, then $\C = \D_0 \otimes \D_1 \leq_T \D_1 $, a contradiction.
\end{proof}
