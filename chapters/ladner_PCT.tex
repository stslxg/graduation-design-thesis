\chapter{Ladner Theorem in Parameterized Complexity Theory}
Results similar with Ladner Theorem in classical complexity theory will be proven in this chapter for parameterized complexity theory, showing that the $W$-hierarchy in parameterized complexity is also dense, as the polynomial hierarchy in classical complexity, with respect to both $\FPT$ m-reduction and $\FPT$ Turing-reduction. Therefore intuitively we can say the structure of parameterized complexity classes and that of classical one have something in common essentially, although the former one was first defined by closure of reduction \cite[104]{flum06}(machine characterization was given later\cite[177]{chen05}) while the latter one was first defined by machine characterization\cite[39]{arora09}.
\section{The Theorem}

Following is the parameterized version of Ladner Theorem and its general version.
\begin{theorem} \label{thm:ladpct}
Assuming $\FPT \neq \WOne$, there is a parameterized problem $\C(\la x, k \ra) \in \WOne$ that is neither $\FPT$ nor $\WOneCompl$.
\end{theorem}
\begin{theorem} \label{thm:ladpcts}
For any non-trivial recursive sets $\A <_m^{\FPT} \B$, there is a set $\C$ such that $\A <_m^{\FPT} \C <_m^{\FPT} \B$.
For any recursive sets $\A <_T^{\FPT} \B$, there is a set $\C$ such that $\A <_T^{\FPT} \C <_T^{\FPT} \B$.
\end{theorem}

\section{Sketch of The Proof}
\subsection{Notations}
In the following parts, I will use $\la \cdot, \cdot \ra$ for the standard pairing function, and define $\la \cdot, \cdot, \cdot\ra = \la \la \cdot, \cdot \ra, \cdot \ra$, $\la \cdot, \cdot, \cdot, 0\ra = 2\la \cdot, \cdot, \cdot\ra$, $\la \cdot, \cdot, \cdot, 1\ra = 2\la \cdot, \cdot, \cdot \ra +1$.

$\phi_e(x)$ is the $e$-th Turing Machine with input $x$, given by standard enumeration, $\phi_{e,s}(x)$ is its computation upto $s$ steps. $\Psi_e(\la x,k \ra)$ is the $e$-th parameterized Turing Machine with input $x$, parameter $k$.

Note that all the sets discussed here are recursive. Denote $\A^{(k)}= \{\la x, k' \ra | \la x, k' \ra \in \A \mbox{ and } k' = k\}$, $\A^{(\leq k)} = \bigcup_{k' \leq k} A^{(k')} = \{\la x, k' \ra | \la x, k' \ra \in \A \mbox{ and } k' \leq k\}$. Besides, $\A^{(\leq k)}$ will refuse to answer $\la x',k' \ra$ with $k' > k$, which can be implemented simply by returning values other than $0$ and $1$.

Denote $\A_s$ as the function of simulating the program for $\A$ up to $s$ steps. The operator `$\neq$' will be satisfied only when its operands are converged and unequal.

\subsection{Discussion}
In this proof, given a non-trivial $\FPT$ problem $\A$ and a $\WOneCompl$ parameterized problem $\B$, I will construct a parameterized problem $\C \in \WOne$ such that 
\begin{equation}\left\{\begin{array}{ll}
 \C & \not\in \FPT \\
 \C & \not \in \WOneCompl
\end{array},\right.
\end{equation}
that is to say, by definition of $\FPT$ and $\WOneCompl$, the constructed set $\C$ should satisfy that $\C \leq^{\FPT}_m \B$ and 
\begin{equation}
\left\{\begin{array}{ll}
  \C \not\leq^{\FPT}_m \A \\
  \B \not\leq^{\FPT}_m \C  
\end{array}.\right.\end{equation}
For the first condition, the detailed requirements are the following:
  \begin{equation} \begin{array}{ll}
      R_{\la e_1,e_2,n,0 \ra} \colon & \exists k \exists s \exists x \text{ such that }\\
        & \left(\left. \mbox{or}
          \begin{array}{ll}
              \phi_{e_1}(k) \uparrow\\
              \left( \left. \phi_{e_1,s}(k) \downarrow \mbox{ and } 
                 \left( \left. \mbox{or}
                     \begin{array}{l}
                         \Psi_{e_2}(\la x,k \ra) \not\downarrow \mbox{ in } \phi_{e_1}(k) |x|^n \mbox{ steps } \\
                         \A^{(\leq \phi_{e_1}(k))}(\Psi_{e_2}(\la x, k \ra)) \neq \C(\la x, k \ra)
                     \end{array}
                 \right. \right)
              \right.\right)
          \end{array} \right.\right)
     \end{array}
  \end{equation}
For the second condition, the detailed requirements are the following:
  \begin{equation} \begin{array}{ll}
      R_{\la e_1,e_2,n,1 \ra}\colon & \exists k \exists s \exists x \text{ such that }\\
       & \left(\left. \mbox{or}
          \begin{array}{ll}
              \phi_{e_1}(k) \uparrow\\
              \left( \left. \phi_{e_1,s}(k) \downarrow \mbox{ and } 
                 \left( \left. \mbox{or}
                     \begin{array}{l}
                         \Psi_{e_2}(\la x,k \ra) \not\downarrow \mbox{ in } \phi_{e_1}(k) |x|^n \mbox{ steps } \\
                         \C^{(\leq \phi_{e_1}(k))}(\Psi_{e_2}(\la x,k \ra)) \neq \B(\la x,k \ra)
                     \end{array}
                 \right. \right)
              \right.\right)
          \end{array} \right.\right)
     \end{array}
  \end{equation}

Similar as what we done in the proof of original Ladner Theorem in classical complexity theory, when construing $\C$ \emph{stage by stage}, we will imitate $\B$ to make $\C$ `harder' than $\A$, thus satisfying requirements $R_{\la e_1,e_2,n,0 \ra}$, while setting $0$'s in $\C$ just like ``blowing holes'' on $\B$ to make $\C$ `easier' than $\B$, thus satisfying requirements $R_{\la e_1,e_2,n,1 \ra}$. However, we have two dimensions needed to treat differently in the construction of $\C$, say the $x$ dimension and the $k$ dimension, and we don't know whether $\phi_{e_1}(k)$ converges when constructing thus cannot enumerate all and only $\FPT$ m-reductions, which brings lots of difficulties to this proof.

The resulting proof combines the ``blowing holes'' technique used in the original proof and the finite injury priority method to overcome such difficulties. Roughly speaking, we will conduct construction like ``blowing holes'' mainly in the $x$ dimension, while using finite injury priority method in the $k$ dimension.

More precisely, at each stage $s$ we will set elements $\la x,k \ra$ of set $\C$ with $|x| = s$ as in the original proof. We have a check list of requirements, with each requirement $R_{\QE}$ asserting control of several consecutive rows of $\C$ in the $k$ dimension between $r'(\QE)$ exclusively and $r(\QE)$ inclusively,  according to their priorities, where $r'(\QE)$ and $r(\QE)$ will be maintained at each stage for each requirement $R_{\QE}$. Then $\la x,k \ra$ is set to $0$ or some element of $\B$ at this stage according to the requirement under which it is controlled.

$\phi_{e_1}(k)$ should be checked to converge before we do further checking of that requirement. Hence for each requirement, a counter $k(\QE)$ will be maintained. Whenever $\phi_{e_1}(k(\QE))$ is tested to be convergent in $s$ steps at each stage $s$, this counter will be increased by one, testing convergence of $\phi_{e_1}$ for the next $k$. All the requirements with total $\phi_{e_1}$ can pass this kind of checking. Although it doesn't hold conversely, it has no influence on our proof as we will just satisfy more requirements.

It is a natural idea that each requirement should control some rows of $\C$ in the $k$ dimension in an attempt to make itself satisfied. $r(\QE)$, the so-called restraint function, is used by each requirement, denoting the rows upto which it attempt to control. For requirements $R_{\la e_1,e_2,n,1 \ra}$ with $i = 1$, we will try to make the first $\phi_{e_1}(k(\QE))$ rows of $\C$ satisfy certain property. Hence if $\phi_{e_1}(k(\QE))$ is tested to be convergent at that stage, we will set $r(\QE)$ to it,  attempting to assert control of those rows. For those with $i = 0$, we will try to make the first $k(\QE)$ rows of $\C$ satisfy certain property. Hence if $\phi_{e_1}(k(\QE))$ is tested to be convergent at that stage, we will set $r(\QE)$ into $k(\QE)$.

To make the constructed set $\C \in \WOne$, we have to add requirements into the check list slowly, using a similar condition with `$(\log s)^n \geq s$' that is used in the proof of the original Ladner Theorem. Then the running time of checking requirements can be bounded as we want, making $\C \leq^{\FPT}_m \B$, thus in $\WOne$.

In summary, in the construction of $\C$, at each stage $s$ we will do the following phases one by one: 
\begin{enumerate}
  \item checking phase: for each requirement in the check list, we check whether it is satisfied, and remove it if so;
  \item updating phase: test for each requirements in the check list that whether its corresponding $\phi_{e_1}(k)$ converges in $s$ steps, and update its $r$ value if so;
  \item adding phase: add requirements to the check list slowly;
  \item setting phase: set $\la x, k \ra$ of $\C$ with $|x| = s$, according to the requirement that controls them.
\end{enumerate}

In this construction, without lost of generality we only consider requirements with strictly increasing $\phi_{e_1}$ by deleting the requirement if we get a $\phi_{e_1}(k(\QE)) \leq \phi_{e_1}(k(\QE)-1)$ in the updating phase. Therefore if $\phi_{e_1}(k(\QE))$ is tested to converge, this requirement can assert control of more rows in the $k$ dimension in both cases, if it has enough priority. This leads the most essential part of this proof.

The most important property of this construction is that a requirement cannot control infinite rows of $\C$, i.e. there must be a stage that after it this requirement cannot control more rows in the $k$ dimension. Mathematical induction on $\QE$ is needed to prove this, which is the most complicated and essential part of the proof. Intuitively speaking, if a requirement can control infinite rows of $\C$, then it never fail the checking phase and $\phi_{e_1}$ is total. There are two different cases as follow.

If we are in such a requirement that we are imitating rows of $\B$ in constructing $\C$, then we have an $\FPT$ m-reduction from $\C$ to $\A$ by the checking phase. If we take special attention in imitating rows of $\B$ so that we shift all the rows of $\B$ down into the region of rows of $\C$ that this requirement has enough priority to control, then we can deduce an $\FPT$ m-reduction from $\B$ to $\A$. Therefore it lead to contradiction with the assumption that $\FPT \neq \WOne$.

Otherwise we are in such a requirement that we are setting $0$'s in constructing $\C$, then we have an $\FPT$ m-reduction from $\B$ to $\C$ by the checking phase. For the rows of $\C$ it controls, we can easily get an $\FPT$ algorithm for calculating $\C$. By induction hypothesis we can have a stage such that from then on all the requirements before this one don't change the rows their control. Therefore for the rows it doesn't control, we may have a requirement of $R_{\la e_1, e_2, n,0 \ra}$'s kind with higher priority so that it has the largest value of restraint function eventually and permanently after that stage. Then by checking phase of that requirement, we have an $\FPT$ m-reduction from $\C$ to $\A$ for the rows upto its restraint function's value. For the rows between them, it must be all $0$'s in $\C$ except for certain elements at the beginning. Hence by combining this three parts, we can get an $\FPT$ m-reduction from $\B$ to $\A$. Then it leads to the same contradiction again.

In the following section, I will present the precise proof, which is a formal and rigorous version of this sketch.

\section{The Proof}
\subsection{Construction of $\C$}
    In the construction we will maintain a number $u$ for slowly adding requirements, a set $\ACT$ recording all the requirements needed to check. For each requirement $R_{\QE}$, we will maintained a counter $k(\QE)$ and its restraint function's value $r(\QE)$. We also maintain a value $r'(\QE)$ for each requirement for convenience, calculated by the $r(\QE)$'s of requirements with higher priority than it. Values of $r(\QE)$ and $r'(\QE)$ are in $\N \cup \{-1\}$.
\subparagraph*{Stage $0$: }
    Set $\ACT \leftarrow \varnothing$, $u \leftarrow 0$.
\subparagraph*{Stage $s$: } 
    Execute the following commands phase by phase:
\begin{enumerate}
    \item \emph{(Checking phase)}$\forall \QE \in \ACT$, 
        \begin{itemize}
            \item \emph{If $i = 0$:} Enumerate all $\la x', k'\ra $ such that $|x'| < \log s$ and $k' < k(\QE)$, check whether the following condition ever holds:
\begin{equation} \left. \mbox{or}
      \begin{array}{l}
          \Psi_{e_2}(\la x',k' \ra) \not\downarrow \mbox{ in } \phi_{e_1}(k') |x'|^n \mbox{ steps } \\
          \A^{(\leq \phi_{e_1}(k'))}_s(\Psi_{e_2}(\la x', k' \ra)) \neq \C_s(\la x', k' \ra)
      \end{array}
   \right..\end{equation}
 If so, set $\ACT \leftarrow \ACT \setminus \la e_1, e_2, n, i\ra$, and we say requirement $R_{\QE}$ \emph{is removed as finitely satisfied}.
            \item \emph{If $i = 1$:} Enumerate all $\la x', k'\ra $ such that $|x'| < \log s$ and $k' < k(\QE)$, check whether the following condition ever holds:
\begin{equation} \left. \mbox{or}
       \begin{array}{l}
           \Psi_{e_2}(\la x',k' \ra) \not\downarrow \mbox{ in } \phi_{e_1}(k') |x'|^n \mbox{ steps } \\
           \left( \left. \mbox{and}
               \begin{array}{c}
                   \Psi_{e_2}(\la x', k' \ra) = \la x'', k'' \ra \mbox{ and } |x''| < \log s \\
                   \C^{(\leq \phi_{e_1}(k'))}_s(\la x'', k'' \ra) \neq \B_s(\la x',k' \ra)
                \end{array}
           \right.\right)
       \end{array}
   \right. .\end{equation}
 If so, set $\ACT \leftarrow \ACT \setminus \QE$, and we say requirement $R_{\QE}$ \emph{is removed as finitely satisfied}.
        \end{itemize}

        The $\C_s(\la x', k'\ra)$ in the this phase denotes the function obtained by either returning $0$ immediately, or simulating $\B$ on the corresponding input for $s$ steps, according to what element we set in the setting phase for $\C(\la x',k'\ra)$.

    \item \emph{(Updating phase)} Let 
      \begin{equation} \mathcal{S}_{recv} = \{ \QE | \phi_{e_1,s}(k(\QE)) \downarrow \mbox{ and } \QE \in \ACT\}.\end{equation}  If $\mathcal{S}_{recv} \neq \varnothing$, then $\forall \QE \in \mathcal{S}_{recv}$:
            \begin{itemize}
                \item \emph{If $\phi_{e_1}(k(\QE)) \leq \phi_{e_1}(k(\QE)-1)$ with $k(\QE) \geq 1$:} Set $\ACT \leftarrow \ACT \setminus \{\QE\}$, and we say requirement $R_{\QE}$ \emph{is removed as being bounded}.
                \item \emph{Otherwise:} 
                  \begin{itemize}
                    \item \emph{If $i = 0$:} Set $r(\QE) \leftarrow k(\QE)$ and $k(\QE) \leftarrow k(\QE) + 1$.
                    \item \emph{If $i = 1$:} Set $r(\QE) \leftarrow \phi_{e_1}(k(\QE))$ and $k(\QE) \leftarrow k(\QE) + 1$.
                  \end{itemize}
            \end{itemize}
    \item \emph{(Adding phase)}
        \begin{itemize}
            \item \emph{If $(\log s)^u > s$:} do nothing.
            \item \emph{Otherwise:} for $\QE = u$, set $\ACT \leftarrow \ACT \cup \{\QE \}$, $k(\QE) \leftarrow 0$ and $r(\QE) \leftarrow -1$. Then set $u \leftarrow u + 1$.
        \end{itemize}
    \item \emph{(Setting phase)} $\forall \QE \in \ACT$, let
        \begin{equation} \mathcal{S}_{down} = \{\QEp | \QEp < \QE \mbox{ and } \QEp \ra \in \ACT\},\end{equation} set 
        \begin{equation}r'(\QE) \leftarrow \left \{ \begin{array}{ll}
            \max_{ \QEp \in \mathcal{S}_{down}}\{r(\QEp)\} & \mbox{ if } \mathcal{S}_{down} \neq \varnothing \mbox{ , }\\
            -1 & \mbox{ otherwise}
        \end{array}\right. . \end{equation}
     If $r'(\QE) < r(\QE)$, then $\forall \la x',k' \ra $ such that $|x'| = s$ and $ r'(\QE) + 1 \leq k' \leq r(\QE )$, 
     \begin{itemize}
         \item \emph{If $i=0$:} set $\C(\la x',k' \ra) \leftarrow \B(\la x', k'-r'(\QE)-1\ra)$.
         \item \emph{If $i=1$:} set $\C(\la x',k' \ra) \leftarrow 0$.
     \end{itemize}
\end{enumerate}

\subsection{Proof of Correctness}
The above construction is obviously well-defined and recursive.

For convenience of our proof, let $r_s(\QE)$ denote the value of $r(\QE)$ right after stage $s$, and similarly $r'_s(\QE)$ of $r'(\QE)$, $k_s(\QE)$ of $k(\QE)$, $u_s$ of $u$. If the requirement $R_{\QE}$ is removed at some stage, we will keep their values to all the following stages. Let $\ACT_s$ denote the content of the set $\ACT$ right after stage $s$. We say a requirement $R_{\QE}$ is \emph{never removed} iff $\exists$ stage $s$ such that $\forall t \geq s, \QE \in \ACT_t$.
\begin{lemma}
    $\lim_{s \to \infty} u_s = \infty$, i.e. we will add all the $\QE$'s into $\ACT$.
\end{lemma}
\begin{proof}
For fixed $u$, since $s$ grows much faster than $\log s$ with respect to $s$, there will be stage $t$ such that $(\log t)^u \leq t$. Therefore according to the adding phase, we will enumerate $\QE$ with $\QE = u$ into $\ACT$ at stage $t$ and also increase $u$. Then by induction we can prove this lemma.
\end{proof}
\begin{lemma}
    $\forall \QE$, we have the following statements hold:
    \begin{enumerate}[(i)]
        \item If requirement $R_{\QE}$ is never removed, there exists a stage $s'(\QE)$ such that $\forall s \geq s'(\QE)$ , $ r'_s(\QE) = r'_{s'(\QE)}(\QE)$.
        \item If requirement $R_{\QE}$ is removed as being bounded, then $\phi_{e_1}$ is not strictly increasing.
        \item $r_s(\QE)$ is non-decreasing with respect to $s$.
        \item \label{lem:finite} $r_s(\QE)$ is finite, i.e. there exists a bound $c(\QE)$ such that $\forall s$, $r_s(\QE) \leq c(\QE)$.
        \item There exists a stage $s(\QE) \geq s'(\QE)$ such that $\forall t \geq s(\QE)$, $r_{t}(\QE) = r_{s(\QE)}(\QE)$.
        \item If requirement $R_{\QE}$ with $i = 0$ is never removed, then we have an $\FPT$ m-reduction from $\C$ to $\A$ for the first $r_{s(\QE)}(\QE)$ rows, i.e. $\forall k' \leq r_{s(\QE)}(\QE)$, $\forall x'$, $\phi_{e_1}(k') \downarrow$, $\Psi_{e_2}(\la x', k'\ra) \downarrow$ in $\phi_{e_1}(k')|x'|^{n}$ steps, and $\A^{(\leq \phi_{e_1}(k'))}(\Psi_{e_2}(\la x',k' \ra)) = \C(\la x',k' \ra)$.
        \item Requirement $R_{\QE}$ are satisfied, except for requirement with $\phi_{e_1}$ non-strictly-increasing.
    \end{enumerate}
\end{lemma}
\begin{proof}
    We will prove these statements by induction on $\QE$. 
    
    \textsf{INDUCTION:} Suppose $\forall \QEp < \QE$, the above statements hold.
    \begin{enumerate}[(i)]
        \item By induction hypothesis, $\forall \QEp < \QE$, $s(\QEp)$ exist. By definition of $r'$, $s'(\QE) = \max_{\QEp < \QE}(s(\QEp))$. Since there are only finite many $\QEp$'s, $s'(\QE)$ exists.
        \item It can only occur in the updating phase. Therefore there exists stage $s$ with $\phi_{e_1}(k_s(\QE)) \leq \phi_{e_1}(k_s(\QE) -1)$, hence $\phi_{e_1}$ is not strictly increasing. 
        \item In the construction, we only update $r(\QE)$ to $\phi_{e_1}(k(\QE))$ to a greater value in the updating phase for both cases.
        \item 
          If $R_{\QE}$ is removed at stage $s$, then obviously take $c(\QE) = r_s(\QE)$ as a bound since $r_s(\QE)$ is non-decreasing.

          If $R_{\QE}$ is never removed, we will prove it by contradiction. Assume $r_s(\QE)$ has no upperbound, it must be the case that $\lim_{s \to \infty} k_s(\QE) = \infty$. Hence $\phi_{e_1}$ is total and $\forall k$, $\phi_{e_1}(k) \geq k$. Therefore there is a function \begin{equation} h(k) = \min\{s \geq s'(\QE) |r_s(\QE) \geq k\},\end{equation} which is computable in $k$ given $s'(\QE)$, by simply running the above construction. Since $r_s(\QE)$ is non-decreasing, $h(k)$ is non-decreasing. 
          
          Since $\A$ is non-trivial, there must be $\la x_0, k_0 \ra$ such that $\A(\la x_0, k_0 \ra) = 0$ and $\la x_1, k_1 \ra$ such that $\A(\la x_1, k_1 \ra) = 1$. 

          Then we have the following situations:
              		\begin{itemize}
                  		\item \emph{If $i = 0$:} according to the checking phase, since $\A$ and $\C$ are recursive sets, we have an $\FPT$ m-reduction from $\C$ to $\A$, i.e.
                      		\begin{align} \label{eqn:cor1}
                          		\forall k', x', & ~\phi_{e_1}(k') \downarrow, \Psi_{e_2}(\la x', k'\ra) \downarrow \text{ in $\phi_{e_1}(k')|x'|^n$ steps } \notag \\
                          		\text{ and } & \A^{(\leq \phi_{e_1}(k'))}(\Psi_{e_2}(\la x',k' \ra)) = \C(\la x',k' \ra).
                      		\end{align}
                      		
                                According to the setting phase, since $h(k') \geq s'(\QE)$ for all $k'$, we have
                     	 	\begin{align} \label{eqn:cor2}
                     	 		\forall k' > & ~r'_{s'(\QE)}(\QE),  \forall x' \text{ such that } |x'| \geq h(k'), \notag \\
                     	 		& \C(\la x', k'\ra) = \B(\la x', k'-r'_{s'(\QE)}(\QE)-1\ra).
                     	 	\end{align}
                      		
                                Then following is an m-reduction from $\B$ to $\A$ on input $\la x', k' \ra$:
                      		\begin{itemize}
                      			\item Calculate $k'' = k' + r'_{s'(\QE)}(\QE) + 1$.
                                        \item 
                                          \begin{itemize}
                                            \item \emph{If $|x'| \geq h(k'')$:} output $\Psi_{e_2}(\la x', k'' \ra)$.
                                            \item \emph{Otherwise:} directly run the original program computing $\B(\la x', k' \ra)$, then output $\la x_0, k_0 \ra$ if the result is $0$, and $\la x_1, k_1 \ra$ if the result is $1$.
                                          \end{itemize}
                      		\end{itemize}
                      		Correctness of the above m-reduction is obvious by equations \ref{eqn:cor1} and \ref{eqn:cor2}, as we are shifting all rows of $\B$ down into region of rows in $\C$ starting from $r'_{s'(\QE)}(\QE) +1$. 

                                Assume the result computed is $\la x''', k''' \ra$. In the first case, $k'''$ is bounded by $\phi_{e_1}(k'') = \phi_{e_1}(k' + r'_{s'(\QE)}(\QE) + 1)$, which is a computable function in $k'$ given $r'_{s'(\QE)}(\QE)$. The running time of this case is bounded by $\phi_{e_1}(k' + r'_{s'(\QE)}(\QE) + 1)|x'|^n$. Obviously the $k'''$ computed by the second case is bounded. The running time of the second case is bounded by function computable in $h(k')$ thus also in $k'$, since there are only $O(2^{h(k')})$-many $x'$'s falling into this case for each $k'$. Hence above is an $\FPT$ m-reduction from $\B$ to $\A$.
                      		
                      		Therefore $\B \leq^{\FPT}_m \A$, thus $\B \in \FPT$, contradiction to the assumption that $\FPT \neq \WOne$.
                  		\item \emph{If $i = 1$:} according to the checking phase, since $\B$ and $\C$ are recursive sets, we have an $\FPT$ m-reduction from $\B$ to $\C$, i.e. 
                      		\begin{align} \label{eqn:cor3}
                          		\forall k', x', & ~\Psi_{e_2}(\la x', k'\ra) \downarrow \text{ in $\phi_{e_1}(k')|x'|^n$ steps } \notag\\
                          		\text{ and } & \C^{(\leq \phi_{e_1}(k'))}(\Psi_{e_2}(\la x',k' \ra)) = \B(\la x',k' \ra).
                     	 	\end{align}

                     	 	According to the setting phase, since $h(k') \geq s'(\QE)$ for all $k'$, we have
                     	 	\begin{align} \label{eqn:cor4}
                     	 		\forall k' > r'_{s'(\QE)}(\QE), \forall x' \text{ such that } |x'| \geq h(k'), ~\C(\la x', k'\ra) = 0.
                     	 	\end{align}

                                Let \begin{equation} \label{eqn:qep}
                                  \QEp = \arg\max_{\QEp} \{r_{s'(\QE)}(\QEp)\}
                                \end{equation}
                                subject to conditions
                                \begin{equation}
                                  \left\{ \begin{array}{c}
                                    i' = 0 \\
                                    \QEp < \QE \\
                                    \forall t \geq s'(\QE), \QEp \in \ACT_t
                                  \end{array}\right. .
                                \end{equation}
                                
                                By definition and induction we have 
                                \begin{align} & \forall \QEp  < \QE,  \forall k' > r'_{s'(\QE)}(\QE),  \notag \\
                                  & h(k') \geq s'(\QE) \geq s(\QEp) \geq s'(\QEp), \end{align} 
                                i.e. rows actually controlled by each $\QEp$'s with priority higher than $\QE$ doesn't change any more after stage $h(r'_{s'(\QE)}(\QE)+1)$.

                                If the $\QEp$ in equation \ref{eqn:qep} exists, we have 
                                \begin{align} \label{eqn:cor5}
                                  &\forall k' \mbox{ such that } r_{s'(\QE)}(\QEp) < k' \leq r'_{s'(\QE)}(\QE), \notag \\
                                  & \forall x'\mbox{ such that } |x'| \geq h(r'_{s'(\QE)}(\QE) + 1),  \notag \\
                                  & \C(\la x', k'\ra) = 0. \end{align}
                                Besides, by induction hypothesis we can also have an $\FPT$ m-reduction from $\C$ to $\A$ for the first $r_{s'(\QE)}(\QEp)$ rows, i.e. 
                                \begin{align} \label{eqn:cor6}
                                  & \forall k' \leq r_{s'(\QE)}(\QEp), \forall x', \notag \\
                                  &\phi_{e'_1}(k') \downarrow, \Psi_{e'_2}(\la x', k'\ra) \downarrow \mbox{ in $\phi_{e'_1}(k')|x'|^{n'}$ steps }, \notag \\
                                  & \mbox{ and } \A^{(\leq \phi_{e'_1}(k'))}(\Psi_{e'_2}(\la x',k' \ra)) = \C(\la x',k' \ra).
                                \end{align}

                      		Then following is an m-reduction from $\B$ to $\A$ on input $\la x', k' \ra$:
                      		\begin{enumerate}
                      		    \item Calculate $\la x'', k'' \ra = \Psi_{e_2}(\la x', k'\ra)$.
                      		    \item \label{algo:FPT-B}
                      		        \begin{itemize}
                      		            \item \emph{If $k'' > r_{s'(\QE)}(\QEp)$:}	
                      		            	\begin{itemize}
                      		            		\item \emph{If  ($k'' \leq r'_{s'(\QE)}(\QE)$ and $|x''| \geq h(r'_{s'(\QE)}(\QE)+1)$) or ($k'' > r'_{s'(\QE)}(\QE)$ and $|x''| \geq h(k'')$):} output $\la x_0, k_0 \ra$.
                      		            		\item \emph{Otherwise:} directly run the above construction computing $\C(\la x', k' \ra)$, then output $\la x_0, k_0 \ra$ if the result is $0$, and $\la x_1, k_1 \ra$ if the result is $1$.
                      		            	\end{itemize}
                      		            \item \emph{Otherwise:} output $\Psi_{e'_2}(\la x'', k'' \ra)$.
                      		        \end{itemize}
                      		\end{enumerate}
                      		Correctness of the above m-reduction is obvious by equations \ref{eqn:cor3}, \ref{eqn:cor4}, \ref{eqn:cor5} and \ref{eqn:cor6}. $\la x', k' \ra$ of $\B$ is first mapped into an element of $\C$, then mapped into $\A$ accordingly.

                                Assume the result is $\la x''', k'''\ra$. In the first case of (b), $k'''$ is obviously bounded. The worst running time of the first case is bounded by function computable in $h(k'')$ or $h(r'_{s'(\QE)}(\QE)+1)$ similarly as in the previous situation, thus bounded by function computable in $k'$ since $k'' \leq \phi_{e_1}(k')$, regardless of $x'$. For the second case of (b), $\FPT$ m-reductions are close under composition. More precisely, the computed $k''' \leq \phi_{e'_1}(k'')$, and thus $k''' \leq \phi_{e'_1}(\phi_{e_1}(k'))$, which is a function computable in $k'$, as $\phi_{e'_1}$ is strict increasing. The running time of computing $\Psi_{e'_2}(\Psi_{e_2}(\la x', k'\ra))$ is bounded by some $\phi(k')|x'|^m$. Hence above is an $\FPT$ m-reduction from $\B$ to $\A$.
                      		
                                If the $\QEp$ in equation \ref{eqn:qep} doesn't exists, then it is straightforward to get an $\FPT$ m-reduction from $\B$ to $\A$ similarly.
                               
                      		Therefore $\B \in \FPT$, contradiction to the assumption that $\FPT \neq \WOne$.
              		\end{itemize}
        \item Trivial by the above two statements.
        \item $k_s(\QE)$ has an upperbound, thus $\phi_{e_1}(k_{s(\QE)}(\QE)) \uparrow$. Since the requirement is never removed as finitely satisfied, an $\FPT$ m-reduction from $\C$ to $\A$ on these rows can be obtained as it never fails the checking phase, similarly as in the proof of the first situation of statement \ref{lem:finite}. Notice that for requirement $R_{\QE}$ with $i = 0$ we always have $r_s(\QE) = k_s(\QE) - 1$ for any stage $s$ by updating phase, hence condition $k' < k(\QE)$ is equivalent to condition $k' \leq r(\QE)$ in all stages.
        \item If requirement $R_{\QE}$ is not removed as being bounded, then it can be the following situations:
                  \begin{itemize}
                      \item \emph{$R_{\QE}$ is never removed:} then we've found $k_{s(\QE)}(\QE)$ as a witness for non-totality of $\phi_{e_1}$, hence $R_{\QE}$ is satisfied.
                      \item \emph{$R_{\QE}$ is removed as finitely satisfied:} then we've found witness $\la x', k'\ra$ for requirement $R_{\QE}$.
                  \end{itemize}        
    \end{enumerate}
    \textsf{BASICS:} For $\QE = 0$, $r'_s(\QE) = -1$ for all stage $s$, and $s'(\QE) = 0$. Then all proofs for remaining statements are similar as above.
\end{proof}
\begin{lemma}
    $\C \leq^{\FPT}_m \B$, hence $\C \in \WOne$.
\end{lemma}
\begin{proof}
Obviously $\B$ is non-trivial, hence there must be $\la x_0, k_0 \ra$ such that $\B(\la x_0, k_0 \ra) = 0$. On input $\la x', k' \ra$, run the construction described above to stage $|x'|$, output $\la x', k' - r'(\QE) - 1\ra$ if $i = 0$ and $\la x_0, k_0 \ra$ if $i = 1$. Then it is a m-reduction with output parameter bounded by computable function $\phi(k) = k$. 

Observe that in the setting phase, there is no need to set all the value of $\C$ before stage $|x'|$. We can store the current $k$, $r$ and $r'$ values for $\QE$ currently in $\ACT$, then reproduce values of $\C$ in the checking phase on demand. Hence the setting phase can be done in polynomial time. In the checking phase, at stage $s$ for each $\QE$, we only enumerate $2^{\log s} = s$ many $x'$'s and computably many $k'$'s, and for each $x'$, $k'$, we can simulate the Turing Machine in time $(\log s)^n \leq (\log s)^{u_s} \leq s$ (since we're adding $\QE$'s very slowly, according to the adding phase we have $(\log s)^{u_s} \leq s$ asymptotically) times function computable in $k'$, with logarithmic overhead. Besides, we only simulate programs for $\A$ and $\B$ for $s$ steps. Therefore the running time of the checking phase is bounded in some $\phi(k')|x'|^m$. Obviously, the remaining phases can be done in polynomial time. Therefore the whole reduction can be done in time bounded by some $\phi(k')|x'|^m$, i.e. it is an $\FPT$ m-reduction from $\C$ to $\B$.
\end{proof}

    Following is the proof of Theorem \ref{thm:ladpct}.
\begin{proof}
    It is obvious that $\FPT$ m-reduction with restriction on strictly increasing $\phi_{e_1}$'s are equivalent with the original one, in the sense that if a problem $\A$ can be reduced to a problem $\B$ using the original $\FPT$ m-reduction, then it can also be reduced by some $\FPT$ m-reduction with this restriction. Therefore $\C \not\leq_m^{\FPT} \A$, $\B \not\leq_m^{\FPT} \C$, $\C \leq^{\FPT}_m \B$, i.e. $\C$ is neither $\FPT$ nor $\WOneCompl$, while $\C \in \WOne$.
\end{proof}

In the above proof, we don't use the fact that $\B$ is $\WOneCompl$, except for introducing contradiction to the assumption $\FPT \neq \WOne$ and getting the final result. However, for $\FPT$ m-reduction part of Theorem \ref{thm:ladpcts}, proving $\B \leq^{\FPT}_m \A$ as a contradiction is adequate. Besides, now we need to ensure that the constructed set $\C$ satisfies $\A \leq^{\FPT}_m \C$. We can easily do it by letting the desired set $\C$ be $\C' \oplus \A$, where $\C'$ is constructed by a similar method with the previous proof, with slight modifications that now requirements $R_{\la e_1,e_2,n,1\ra}$ correspond to $\FPT$ m-reductions from $\B$ to $\C' \oplus \A$ and that the $\FPT$ m-reduction from $\B$ to $\A$ made for contradiction in situations that a requirement of this kind controls infinite rows will now have two cases according to where the input is mapped to by the $\FPT$ m-reduction from $\B$ to $\C' \oplus \A$, which can be solved easily by definition of the `$\oplus$' operator. Therefore it is straightforward to see that the above proof can also be used to prove Theorem \ref{thm:ladpcts}, especially for $\FPT$ m-reduction. 

For $\FPT$ Turing-reduction part of Theorem \ref{thm:ladpcts}, slight but easy modifications are needed. All the requirements and reductions we obtained in the proof for contradiction should be changed into Turing-reduction's styles. Besides, we will restrict $\A_s^{\mathcal{O}}$ to run upto $s$ steps including steps taken by the oracle machine. Observe that there is no need to restrict the set $\A$ to be non-trivial, since we can directly output a $\{0,1\}$-value in Turing-reductions. Then similarly we can get this result.
