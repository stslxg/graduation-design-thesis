\chapter{Preliminaries}
In this chapter, I will introduce some basic definitions and theorems needed for the following chapter from computability theory, computational complexity and parameterized complexity.

\section{Computability Theory}
For more details of definitions and theorems in computability theory, especially proof of theorems, please refer to \cite[1]{cutland80} and \cite[1]{homer11}.

\begin{definition}[Problem]
A \emph{problem} is a subset of $\{0,1\}^*$, or equivalently $\N$.
\end{definition}
In the following part, we will use `problem', `language' and `set' interchangeably.

\subsection{Turing Machines}
\begin{definition}[Turing Machine]
 A $k$-tape \emph{Turing Machine}, shorted as TM, is a system
\begin{equation}
  M = \la Q, \Gamma, \Sigma,  \sigma, q_0, N, q_{\textrm{final}} \ra,
\end{equation}
where \\
$Q$ is the finite set of states, \\
$\Gamma$ is the finite tape alphabet, \\
$\Sigma$ is the input alphabet, $\Sigma \subseteq \Gamma \setminus \{B\}$, \\
$B \in \Gamma$ is the blank, \\
$\sigma$ is the transition function of $Q \times \Gamma^k \rightarrow Q \times \Gamma^{k-1} \times \{L,R,S\}^k$, \\
$q_o \in Q$ is the initial state and \\
$q_{\textrm{final}}$ is the final state.
\end{definition}
For details of how a Turing Machine works and \emph{accepts} an input string, please refer to. Intuitively speaking, we will say a Turing Machine \emph{accept} an input if in the final state, the content of its output tape is $1$.
\begin{definition}[Language of Turing Machine]
  Let $M$ be a Turing Machine. The language \emph{accepted} by $M$ is
\begin{equation}
  L(M) = \{ w \in \Sigma^* | M \mbox{ accepts } w \}.
\end{equation}
\end{definition}
For a TM $M$, we denote $M(x) \downarrow$ if the computation of $M$ halts on input $x$, and $M(x) \uparrow$ if it never stops on input $x$. For a function $f$, we also denote $f(x) \downarrow$ if $x \in Domain(f)$ and $f(x) \uparrow$ if not.

Besides, the \emph{function computed by a Turing Machine} $M$ is defined as the content of its output tape whenever it halts for the input, and undefined if it never stops. A function is \emph{computable} iff it is the function computed by some Turing Machine.
\begin{definition}[Recursive sets]
A set $S$ is \emph{decidable}, \emph{computable}, or \emph{recursive}, iff its \emph{characteristic function}
\begin{equation}
  c(x) = \left\{ \begin{array}{ll}
      1, & \mbox{ if } x \in S, \\
      0, & \mbox{ otherwise}
      \end{array} \right.
\end{equation}
is computable by a Turing Machine, denoted as the \emph{program} of set $S$.
\end{definition}

An \emph{relativised TM} $M$ is a TM with an oracle $L$, which has a magical extra tape for $M$, such that whenever $M$ writes a string on this tape and goes into a special ``invocation'' state, then the string gets overwritten in one step by $0$ or $1$ depending on whether the string is in $L$ or not.

\begin{definition}[`$\simeq$']
Let $f$ and $g$ be two functions. Then $f(x) \simeq g(x)$ iff $f(x) = g(x)$ whenever $f(x) \downarrow$, and $f(x) \downarrow$ iff $g(x) \downarrow$. Similarly we define $M(x) \simeq M'(x)$ for two TMs $M$ and $M'$.
\end{definition}
\begin{theorem}[Universal TM]
There is a TM $U$ such that $U(\alpha, x) \simeq M_{\alpha}(x)$, where $M_{\alpha}(x)$ is an TM $M$ with $\alpha$ as its coding.
\end{theorem}
Therefore we can have an effective enumeration of Turing Machines $\{M_k\}_{k \in \N}$ and effective enumeration of computable functions $\{\phi_k\}_{k \in \N}$, where $\phi_k$ is the function computed by $M_k$.
\subsection{Undecidability and Recursively Enumerable Sets}
\begin{definition}[Halting problem]
Halting problem is the problem to decide whether $M_x(y) \downarrow$ or not, with $x$ and $y$ as input.
\end{definition}
\begin{definition}[Undecidability]
A problem $P$ is \emph{undecidable} is it is not decidable.
\end{definition}
\begin{theorem}
Halting problem is undecidable.
\end{theorem}
\begin{definition}[Recursively enumerable sets]
A set $S$ is \emph{partial-decidable}, \emph{partial-computable}, or \emph{recursively enumerable} shorted as \emph{r.e.}, iff its \emph{partial-characteristic function}
\begin{equation}
  \chi(x) = \left\{ \begin{array}{ll}
      1, & \mbox{ if } x \in S, \\
      \mbox{undefined}, & \mbox{ otherwise}
      \end{array} \right.
\end{equation}
is computable by a Turing Machine.
\end{definition}
\begin{theorem}
A set $S$ is r.e. iff there is a \emph{recursive enumeration} $g$ of $S$ such that $S = \{g(k)\}_{k \in \N}$. Moreover, if $S$ is not recursive, $g$ can be injective.
\end{theorem}
\subsection{Reductions}
\begin{definition}[m-reduction]
A set $L$ is \emph{m-reducible} to a set $L'$, denoted by $L \leq_m L'$, if there is a computable function $f:\N \to \N$ such that for every $x \in \N$, $x \in L$ iff $f(x) \in L'$. Then $f$ is called an \emph{m-reduction from $L$ to $L'$}, and we also denote $L \leq_f L'$.
\end{definition}
\begin{definition}[Turing-reduction]
A set $L$ is \emph{Turing-reducible} to a set $L'$, denoted by $L \leq_T L'$, if there is a relativised TM $M$ that, given an oracle for deciding $L'$, can decide $L$. Then this relativised TM is called a \emph{Turing-reduction from $L$ to $L'$}.
\end{definition}
\begin{theorem}
Let $L$ and $L'$ be two sets, then $L \leq_T L'$ iff there exists $e$ such that $L(x) \simeq \phi^{L'}_e(x)$, where $\phi^{L'}_e$ is the function computed by the $e$-th relativised TM with oracle $L'$ and $L(x)$ is the characteristic function of $L$.
\end{theorem}

\section{Computational Complexity Theory}
\subsection{Efficiency and Running Time}
Following definitions and theorems are from Arora's textbook\cite[1]{arora09}.
\begin{definition}
We say a Turing Machine \emph{$M$ computes $f$ in $T(n)$-time} if its computation on every input $x$ requires at most $T(|x|)$ steps and output $f(x)$.
\end{definition}
If a Turing Machine can compute some function in $T(n)$-time where $T(n)$ is a polynomial of $n$, then it is a \emph{polynomial-time TM}.
\begin{definition}[Time-constructible functions]
A function $T: \N \to \N$ is \emph{time constructible} if $T(n) \geq n$ and there is a TM $M$ that computes the function $x  \mapsto \lfloor T(|x|) \rfloor$ in time $T(n)$.
\end{definition}
Examples for time-constructible functions are $n$, $n \log n$, $n^2$ and $2^n$. The TM $M$ in the above definition for time-constructible function $T(n)$ is called \emph{$T(n)$-clocked}.
\begin{theorem}[Efficient universal Turing Machine]
There is a universal TM $U$ so that if the machine it simulates halts on input $x$ within $T$ steps then $U$ halts within $CT \log T$ steps, where $C$ is a constant independent of $x$ and depending only on the TM it simulates.
\end{theorem}
Therefore we can have an effective enumeration of TM in the context of computational complexity.
\begin{definition}[The class $\mathbf{DTIME}$]
Let $T: \N \to \N$ be some function. A language $L$ is in $\mathbf{DTIME}(T(n))$ iff there is a Turing Machine that runs in time $c \cdot T(n)$ for some constant $c > 0$ and decides $L$.
\end{definition}
\begin{definition}[The class $\P$]
$\P = \cup_{c\geq 1}\mathbf{DTIME}(n^c)$.
\end{definition}
Following definitions and theorems are from Papadimitriou's textbook\cite[1]{papa03}.
\begin{definition}[Big $O$ notation]
Let $f$ and $g$ be functions from $\N$ to $\N$. $f(n) = O(g(n))$ iff there are positive integers $c$ and $n_0$ such that, for all $n \geq n_0$, $f(n) \leq c\cdot g(n)$. 
\end{definition}
\begin{theorem}[Linear Speedup Theorem]
Let $L \in \mathbf{DTIME}(f(n))$. Then for any $\epsilon > 0$, $L \in \mathbf{DTIME}(f'(n))$ where $f'(n) = \epsilon \cdot f(n) + n + 2$.
\end{theorem}
\subsection{Reducibility and NP-Completeness}
Following definitions and theorems are from Arora's textbook\cite[1]{arora09}.
\begin{definition}[Polynomial-time m-reduction]
A language $L$ is \emph{polynomial-time m-reducible} to a language $L'$, denoted by $L \leq^{\P}_m L'$, if there is a polynomial-time computable function $f:\{0,1\}^* \to \{0,1\}^*$ such that for every $x \in \{0,1\}^*$, $x \in L$ iff $f(x) \in L'$. Then $f$ is called an \emph{polynomial-time m-reduction from $L$ to $L'$}.
\end{definition}
\begin{definition}[Polynomial-time Turing-reduction]
A language $L$ is \emph{polynomial-time Turing-reducible} to a language $L'$, denoted by $L \leq^{\P}_T L'$, if there is a polynomial-time relativised TM $M$ that, given an oracle for deciding $L'$, can decide $L$. Then this relativised TM is called a \emph{polynomial-time Turing-reduction from $L$ to $L'$}.
\end{definition}
\begin{theorem}
  $\P$ is closed under polynomial-time m-reduction and polynomial-time Turing-reduction, i.e. if $L \leq^{\P} L'$ and $L' \in \P$ then $L \in \P$, where `$\leq^{\P}$' can be `$\leq^{\P}_m$' or `$\leq^{\P}_T$'.
\end{theorem}
\begin{definition}[The class \NP]
A language $L$ is in $\NP$ if there exists a polynomial $p: \N \to \N$ and a polynomial-time TM $M$ such that for every $x \in \{0,1\}^*$, 
\begin{equation}
  x \in L \Leftrightarrow \exists u \in \{0,1\}^{p(|x|)} \mbox{ such that } M(x,u) = 1.
\end{equation}
\end{definition}
\begin{definition}[$\NP$-Hardness and $\NP$-Completeness]
$L'$ is \emph{$\NP$-$\mathsf{Hard}$} if $L \leq^{\P}_m L'$ for every $L \in \NP$. $L'$ is $\NPCompl$ if $L'$ is $\NP$-$\mathsf{Hard}$ and $L' \in \NP$.
\end{definition}
\begin{theorem}
If language $L$ is $\NPCompl$, then $L \in \P$ iff $\P = \NP$.
\end{theorem}
\begin{definition}[$\SAT$]
$\SAT$ is the language of all satisfiable CNF formulae, where CNF formulae are boolean formula over variables $u_1, \ldots, u_n$ for some $n$ that is in Conjunctive Normal Form.
\end{definition}
\begin{theorem}
$\SAT$ is $\NPCompl$.
\end{theorem}
\section{Parameterized Complexity Theory}
The following definitions and theorems are from Flum's book\cite[1]{flum06}.
\subsection{Parameterized Tractability}
\begin{definition}[Parameterized problems]
Let $\Sigma$ be a finite alphabet.
\begin{enumerate}
\item A \emph{parameterization} of $\Sigma^*$ is a mapping $\kappa : \Sigma^* \to \N$ that is polynomial time computable.
\item A \emph{parameterized problem} (over $\Sigma$) is a pair $(Q, \kappa)$ consisting of a set $\Q \subseteq \Sigma^*$ and a parameterization $\kappa$ of $\Sigma^*$.
\end{enumerate}
\end{definition}
For convenience, in the following chapters we will always write a parameterized problem as $Q(\la x, k \ra)$ where $x$ is the input and $k = \kappa(x)$.
\begin{definition}[$\FPT$-algorithm and fixed-parameter tractability]
Let $\Sigma$ be a finite alphabet and $\kappa: \Sigma^* \to \N$ a parameterization.
\begin{enumerate}
\item An algorithm $\A$ with input alphabet $\Sigma$ is an \emph{$\FPT$-algorithm} (with respect to $\kappa$) if there is a computable function $f: \N \to \N$ and a polynomial $p: \N \to \N$ such that for every $x \in \Sigma^*$, the running time of $\A$ on input $x$ is at most $f(\kappa(x)) \cdot p(|x|)$.
\item A parameterized problem $(Q, \kappa)$ is \emph{fixed-parameter tractable} if there is an $\FPT$-algorithm with respect to $\kappa$ that decides $Q$.
\end{enumerate}
\end{definition}
\begin{definition}[The class $\FPT$]
$\FPT$ denotes the class of all fixed-parameter tractable problems.
\end{definition}

\subsection{Reductions under parameterized context}
\begin{definition}[$\FPT$ m-reduction]
Let $(Q,\kappa)$ and $(Q', \kappa')$ be parameterized problems over the alphabet $\Sigma$ and $\Sigma'$, respectively. An \emph{$\FPT$ m-reduction} from $(Q, \kappa)$ to $(Q', \kappa')$, denoted as $(Q, \kappa) \leq^{\FPT}_m (Q', \kappa')$, is a mapping $R: \Sigma^* \to (\Sigma')^*$ such that:
\begin{enumerate}
\item $\forall x \in \Sigma^*$, $x \in Q \Leftrightarrow R(x) \in Q'$.
\item $R$ is computable by an $\FPT$-algorithm.
\item There is a computable function $g: \N \to \N$ such that $\kappa'(R(x)) \leq g(\kappa(x))$ for all $x \in \Sigma^*$.
\end{enumerate}
\end{definition}
\begin{definition}[$\FPT$ Turing-reduction]
Let $(Q,\kappa)$ and $(Q', \kappa')$ be parameterized problems over the alphabet $\Sigma$ and $\Sigma'$, respectively. An \emph{$\FPT$ Turing-reduction} from $(Q, \kappa)$ to $(Q', \kappa')$, denoted as $(Q, \kappa) \leq^{\FPT}_T (Q', \kappa')$, is an algorithm $\A$ with an oracle to $Q'$ such that:
\begin{enumerate}
\item $\A$ decides $(Q, \kappa)$.
\item $\A$ is an $\FPT$-algorithm.
\item There is a computable function $g : \N \to \N$ such that for all oracle queries ``$y \in Q'$?'' posed by $\A$ on input $x$V we have $\kappa'(y) \leq g(\kappa(x))$.
\end{enumerate}
\end{definition}
\begin{theorem}
$\FPT$ is closed under $\FPT$ m-reduction and $\FPT$ Turing-reduction, i.e. if $(Q, \kappa) \leq^{\FPT} (Q', \kappa')$ and $(Q', \kappa') \in \FPT$ then $(Q, \kappa) \in \FPT$, where `$\leq^{\FPT}$' can be `$\leq^{\FPT}_m$' or `$\leq^{\FPT}_T$'.
\end{theorem}
\subsection{W-Hierarchy}
\begin{definition}[$p\text{-WD}_{\psi}$]
Let $\psi(X)$ be a first-order formula with a free relation variable $X$ of arity $s$. Then the parameterized problem $p\text{-WD}_{\psi}$ reads a structure $\A$ and $k \in \N$ as input, $k$ as a parameter, then decides whether there is a relation $S \subseteq \A^*$ of cardinality $|S| = k$ such that $\A \models \psi(S)$.
\end{definition}
\begin{definition}
Let $(Q, \kappa)$ be a parameterized problem. Then we denote $[(Q, \kappa)]^{\FPT}$ as the set $\{(Q', \kappa') | (Q', \kappa') \leq^{\FPT}_m (Q, \kappa)\}$.
\end{definition}
\begin{definition}[The W-Hierarchy]
For every $t \geq 1$, we let $W[t] := [p\text{-WD-}\Pi_t]^{\FPT}$.
\end{definition}
\begin{definition}[$\WOne$-Hardness and $\WOne$-Completeness]
$L'$ is \emph{$\WOne$-$\mathsf{Hard}$} if $L \leq^{\FPT}_m L'$ for every $L \in \WOne$. $L'$ is $\WOneCompl$ if $L'$ is $\WOne$-$\mathsf{Hard}$ and $L' \in \WOne$.
\end{definition}
\begin{theorem}
If language $L$ is $\WOneCompl$, then $L \in \FPT$ iff $\FPT = \WOne$.
\end{theorem}


\section{Miscellaneous}
\begin{definition}
For sets $\A$ and $\B$, $\A \oplus \B = \{2x | x \in \A\} \cup \{2x+1 | x \in \B\}$.
\end{definition}

\begin{definition}
For sets $\A$ and $\B$, $\A < \B$ iff $\A \leq \B$ and $\B \not\leq \B$, where the `$\leq$' relation here can be `$\leq_m$', `$\leq_T$',`$\leq^{\P}_m$',`$\leq^{\P}_T$',`$\leq^{\FPT}_m$' or `$\leq^{\FPT}_T$', with a corresponding version of the `$<$' relation.
\end{definition}

\begin{definition}[Non-triviality]
A set $\A$ is \emph{non-trivial} iff it is neither empty nor full.
\end{definition}

\begin{definition}[Restriction of functions]
  Let $f$ be a function and $S \subseteq \N$ be a set, then $f \upharpoonright S$ is the function defined by \begin{equation}
(f \upharpoonright S)(x) \simeq \left\{ \begin{array}{ll}
  f(x), & \mbox{ if } x \in S, \\
  \mbox{undefined}, & \mbox{ otherwise.}
\end{array} \right.\end{equation}
Moreover, for a number $c$ we define $(f \upharpoonright c)(x) \simeq (f \upharpoonright S)(x)$ with $S = \{x|x\leq c\}$.
\end{definition}
