\chapter{Ladner Theorem}
In this chapter, I will introduce the original Ladner Theorem in classical computational complexity theory and give a concise proof using ``blowing holes'' technique, derived from Ladner's original paper. 
\section{The Theorem}
As we know, which is also included in the ``Preliminaries'' chapter of this thesis, problems in $\P$ can be considered the `easiest' problems in $\NP$, while $\NPCompl$ problems are considered the `hardest' one. An interesting problem comes up naturally: under the assumption that $\P \neq \NP$, are there any problems of `intermediate hardness' in the polynomial degree? Ladner theorem gives an positive answer, and moreover, its corollary gives a much stronger answer by saying that the polynomial degree are dense\cite[158]{ladner75}.

In classical complexity theory, Ladner Theorem reveals the density property of structures of classical complexity classes under plausible assumption $\P \neq \NP$, which is similar to the density property of real numbers. Between any two real numbers, there are infinitely many intermediate real numbers. Similarly, Ladner Theorem demonstrates that between the `easiest' problems and the `hardest' problems in the hierarchy of classical complexity classes, or even between any two problems that are not `equally hard', there are infinitely many intermediate problems that are not `equally hard' with them\cite[160]{ladner75}. 

Following is the original Ladner Theorem and its general version.
\begin{theorem}[Ladner]\label{thm:ladner}
Assuming $\P \neq \NP$, there is an $\NP$ problem that is not $\NPCompl$, and not $\P$.
\end{theorem}

\begin{theorem}\label{thm:ladners}
For any non-trivial recursive sets $\A <_m^P \B$, there is a set $\C$ such that $\A <_m^P \C <_m^P \B$.
For any recursive sets $\A <_T^P \B$, there is a set $\C$ such that $\A <_T^P \C <_T^P \B$.
\end{theorem}

\section{``Blowing Holes'' Proof}
\begin{proof}\textbf{(of Theorem \ref{thm:ladner})}
Fix an effective enumeration of polynomial time-bounded Turing Machines $M_0$, $M_1$, $\cdots$, with each $M_k$ running in time $|x|^k$, and a similar effective enumeration of polynomial-time computable functions $f_0, f_1, \cdots$, with each function $f_k$ computable in time $|x|^k$. 

The problem we are going to attain is $\A = \{ x | x \in \SAT \mbox{ and } g(|x|) \mbox{ is even}\}$, which means we are ``blowing holes'' in a $\NPCompl$ problem. Our construction will ensure that $g(n)$ is computable in polynomial time, therefore $\A \in \NP$.

In order to keep $\A$ `harder' than any problems in $\P$ and `easier' than any problems that is $\NPCompl$ simultaneously, we have the following requirements to meet:
\begin{equation}  P'_e: \A \neq L(M_e) \end{equation}
\begin{equation} N'_e: \SAT \not\leq_{f_e} \A . \end{equation}

A detailed version of requirements in our construction is as follows:
\begin{equation} \begin{array}{ccccccc}
P_e:& &((x \in \SAT  &\mbox{ and } & g(|x|)  \mbox{ is even)} &\mbox{ and }& x \not\in L(M_e)) \\
   & \mbox{ or } & ((  x \not\in \SAT  &\mbox{ or }& g(|x|)  \mbox{ is odd)} &\mbox{ and }& x \in L(M_e)) \end{array} \end{equation}
\begin{equation} \begin{array}{ccccccc}
N_e: &  &(x \in \SAT &\mbox{ and }& (f_e(x) \not\in \SAT & \mbox{ or }& g(|f_e(x)|) \mbox{ is odd})) \\
 & \mbox{ or }& (x \not\in \SAT  &\mbox{ and }& (f_e(x) \in \SAT &  \mbox{ and } & g(|f_e(x)|) \mbox{ is even})).
\end{array} \end{equation}

The whole procedure is as follows: (Since we are using \emph{delayed diagonalization}, the real \emph{stage} in usual concept of diagonalization method is described by the value of $g(s)$ instead of $s$.) 
\paragraph{`Stage' $0$, $1$ and $2$:} Set $g(0) = g(1) = g(2) = 0$.
\paragraph{`Stage' $s+1$ ($s \geq 2$):} Suppose $g(s) = n$. If $(\log \log s)^n \geq \log s$, directly set $g(s+1) = n$; otherwise, we calculate $g(s+1)$ as follow: 
	\begin{itemize}
		\item \emph{$n$ is even}: Let $e = \frac{n}{2}$. Enumerate all $x$ s.t. $|x| \leq \log \log s$, testing whether $P_e$ ever holds. If so, set $g(s+1) = n+1$; otherwise, set $g(s+1) = n$.
		\item \emph{$n$ is odd}:  Let $e = \frac{n - 1}{2}$. Enumerate all $x$ s.t. $|x| \leq \log \log s$, testing whether $N_e$ ever holds. If so, set $g(s+1) = n+1$; otherwise, set $g(s+1) = n$.
	\end{itemize}

We need to prove several useful lemmas first, to show that the above procedure of constructing set $\A$ is well-defined, computable in polynomial time and correct.

\begin{lemma}[Well-definedness]\label{lem:ladner-well}
The above construction of $g(s)$ is well-defined, i.e. $g(s)$ can be computed inductively (i.e. recursively) as described above.
\end{lemma}
\begin{proof}
Since we will directly go to next `stage' $s+1$ whenever $(\log \log s)^n \geq \log s$, asymptotically we have $(\log \log s)^n < \log s$. Therefore
\begin{equation}\label{eqn:ladner}
  |f_e(x)| \leq |x|^e \leq (\log \log s)^e \leq (\log \log s)^n < \log s < s,
\end{equation} the information we need from function $g(s)$ to calculate the $s$-th `stage' lies before this stage.
\end{proof}

\begin{lemma}[Polynomial-time computability]\label{lem:ladner-poly}
$g(s)$ is computable in polynomial time of $s$.
\end{lemma}
\begin{proof}
  Note that we only enumerate $x$ with $|x| \leq \log \log s$ at each `stage' $s$, and by equation \ref{eqn:ladner}, the running time to calculate $g(s)$ inductively is 
  \begin{align}
    T(s) &= T(s-1) + 2^{\log \log s} (O(2^{\log \log s}) + O((\log \log s)^e) + O(2^{(\log \log s)^e})) \notag \\
         &\leq T(s-1) + \log s (O(\log s) + O((\log \log s)^n) + O(2^{(\log \log s)^n})) \notag \\
         &\leq T(s-1) + \log s (O(\log s) + O(\log s) + O(2^{\log s})) \notag \\
         &\leq T(s-1) + O(s^2).
  \end{align}
  $g(s)$ is thus computable in polynomial time of $s$.
\end{proof}

\begin{lemma}[Correctness]\label{lem:ladner-cor}
$g(s)$ is unbounded, i.e. our construction will not get stuck in some real stage $n$ such that there exists a `stage' $s'$, $\forall s \geq s'$, $g(s) = n$.
\end{lemma}
\begin{proof}
  \begin{itemize}
    \item The construction will not get stuck by the condition ``$(\log \log s)^n \geq \log s$'': for fixed $n$, as $\log s$ grows faster than $(\log \log s)^n$, there must be a sufficient large $s$ such that $(\log \log s)^n < \log s$.
    \item It will not get stuck in $n$ such that $n = 2e$: otherwise $\exists s$ such that $\forall |x| \geq s$, $x \in L(M_e)$ iff $x \in \SAT$, hence we can have a polynomial-time algorithm for $\SAT$, contradicting our $\P \neq \NP$ assumption.
    \item It will not get stuck in $n$ such that $n = 2e+1$: otherwise $\exists s$ such that $\forall |x| \geq s$, $x \not\in \A$ and $\forall x$, $x \in \SAT$ iff $f_e(x) \in \A$, hence we have a polynomial-time m-reduction from $\SAT$ to a polynomial-time computable set $\A$, hence $\SAT \in P$, contradiction our $\P \neq \NP$ assumption.
  \end{itemize}
  Therefore $g(s)$ is unbounded.
\end{proof}

By the above lemmas, all the requirements $P_e$'s and $N_e$'s will be checked and satisfied, hence all the $P'_e$'s and $N'_e$'s are satisfied, therefore $\A \not\in \P$ and $\A$ is not $\NPCompl$ while $\A \in \NP$.
\end{proof}

\subsection{An Alternative Proof}
In the above proof, we use the fact that $\SAT$ can be solved in $O(2^{|x|})$ time. However, it is not essential for this kind of proof. In the original paper of Ladner, he use another approach to overcome difficulty of simulating $\SAT$ problem\cite[159]{ladner75}, which is more general and can be used in the proof of its parameterized version.

In addition, we will use $\P$-time m-reductions in both sides of requirements, making the following proof more general to be used in proving Theorem \ref{thm:ladners}.
\begin{proof}\textbf{(of Theorem \ref{thm:ladner})}
Similarly as above, we have an effective enumeration of polynomial time-bounded Turing Machines $\{M_k\}_{k \in \N}$, and an effective enumeration of polynomial-time computable functions $\{f_k\}_{k \in \N}$. 

In addition, we have a non-trivial recursive set $\A \in \P$ and a recursive set $\B$ that is $\NPCompl$. Then there must be $x_0$ such that $\A(x_0) = 0$ and $x_1$ such that $\A(x_1) =1$.

The problem we are going to attain is $\C = \{ x | x \in \B \mbox{ and } g(|x|) \mbox{ is even}\}$, which means we are ``blowing holes'' in $\B$. Our construction will ensure that $g(s)$ is computable in polynomial time of $s$, therefore $\C \leq^{\P}_m \B$ since $\B$ is obviously non-trivial, thus $\C \in \NP$.

Requirements now become the following.
\begin{equation}  P'_e: \C \not\leq_{f_e} \A \end{equation}
\begin{equation} N'_e: \B \not\leq_{f_e} \C . \end{equation}

A detailed version of requirements in our construction is as follow:
\begin{equation}
  P_e:  \C_s(x) \neq \A_s(f_e(x))
\end{equation}
\begin{equation}
 N_e:  \B_s(x) \neq \C_s(f_e(x)),
\end{equation}
where $\A_s$ is the function of simulating the program for $\A$ up to $s$ steps, $\C_s(x)$ is the function obtained by either returning $0$ immediately, or simulating $\B$ on $x$ for $s$ steps, according to the value of $g(|x|)$. The operator `$\neq$' will be satisfied only when its operands are converged and unequal.

The calculation of $g(s)$ is similar as in the previous proof, except that the condition for directly jump into the next `stage' can now be relaxed to $(\log s)^n \geq s$ and we can enumerate all $|x| \leq \log s$.

Well-definedness of this construction is similar since now we have
\begin{equation}
  |f_e(x)| \leq |x|^e \leq (\log s)^e \leq (\log s)^n < s.
\end{equation}

Polynomial-time computability of this construction is also similar since we only simulate $\A$ and $\B$ for up to $s$ steps in each stage $s$.

For correctness of this construction, if it is stuck in $n$ such that $n = 2e$, then we have a $\P$-time m-reduction from $\C$ to $\A$, while $\C$ is almost the same as $\B$ except for finite elements at the beginning. Then we can get an m-reduction from $\B$ to $\A$ as follow: for the beginning part of $\B$, directly calculates them and output $x_0$ if the result is $0$, $x_1$ if it is $1$; for the elements on which $\C$ and $\B$ are the same, use the above m-reduction from $\C$ to $\A$. This reduction obviously runs in polynomial time. Hence we have $\B \leq^{\P}_m \A$, thus $\B \in \P$, contradicting the assumption $\P \neq \NP$.

If the construction is stuck in $n$ such that $n  = 2e+1$, then we have a $\P$-time m-reduction from $\B$ to $\C$, while $\C$ is almost empty except for finite elements at the beginning. Then we can easily get a polynomial-time algorithm for $\B$ as follow: calculates the image of $x$ under the $\P$-time m-reduction from $\B$ to $\C$; if it lies in the beginning part of $\C$, then use this construction and the original program of $\B$ to calculate it; if it lies in the part that $\C$ is empty, then output $0$. This reduction obviously runs in polynomial time. Besides, we can also use this algorithm to get a $\P$-time m-reduction from $\B$ to $\A$ simply by running it and then outputting $x_0$ or $x_1$ accordingly. Hence we also have $\B \leq^{\P}_m \A$ and $\B \in \P$, leading to the same contradiction.

Therefore $\C \not\leq^{\P}_m \A$, $\B \not\leq^{\P}_m \C$ and $\C \leq^{\P}_m \B$, thus $\C$ is neither $\P$ nor $\NPCompl$ while $\C \in \NP$. 
\end{proof}

In the above proof, we don't use the fact that $\B$ is $\NPCompl$, except for introducing contradiction to the assumption $\P \neq \NP$ and getting the finally result. However, for $\P$-time m-reduction part of Theorem \ref{thm:ladners}, proving $\B \leq^{\P}_m \A$ as a contradiction is adequate. Besides, now we need to ensure that the constructed set $\C$ satisfies $\A \leq^{\P}_m \C$. We can easily do it by letting the desired set $\C$ be $\C' \oplus \A$, where $\C'$ is constructed by a similar method with the previous proof, with slight modifications that now $N_e$ becomes $\B_s(x) \neq (\C'_s \oplus \A)(f_e(x))$ and that the polynomial-time m-reduction from $\B$ to $\A$ made for contradiction in situations of getting stuck in $N_e$ will now have two cases according to where the input is mapped to by the polynomial-time m-reduction from $\B$ to $\C' \oplus \A$, which can be solved easily by definition of the `$\oplus$' operator. Therefore it is straightforward to see that the above proof can also be used to prove Theorem \ref{thm:ladners}, especially for $\P$-time m-reduction. 

For $\P$-time Turing-reduction part of Theorem \ref{thm:ladners}, slight but easy modifications are needed. All the requirements and reductions we obtained in the proof for contradiction should be changed into Turing-reduction's styles. Besides, we will restrict $\A_s^{\mathcal{O}}$ to run upto $s$ steps including steps taken by the oracle machine. Observe that there is no need to restrict the set $\A$ to be non-trivial, since we can directly output a $\{0,1\}$-value in Turing-reductions. Then similarly we can get this result.

\section{Further Discussion}
\subsection{Effective Enumeration of Polynomial Time-bounded Turing Machines}
It may be confusing to demonstrate that we can have an effective enumeration of polynomial time-bounded Turing Machines\cite[127]{homer11}, since the following set, which contains \emph{all} polynomial time-bounded TMs,
\begin{equation} PTM = \{ i \vert \mbox{ the }i\mbox{-th Turing Machine halts on all input in polynomial time}\} \end{equation}
is not r.e., otherwise we can solve the Halting problem. 

However, we can have a set $\{M_i\}_i$ s.t. $\P = \cup_i \{L(M_i)\}$ and it is r.e. Of course it doesn't contain \emph{all} polynomial time-bounded TMs, but we don't care the omitting ones, since it is sufficient for our proof. Its existence can be precisely proved as follows:

Let $\{M'_i\}_i$ be a standard effective enumeration of Turing Machines. Let $p_k = n^k + k$, which is obviously \emph{time constructible}. Denote $C_k$ as the $p_k$-clocked TMs. Then we attach $C_k$ to each TMs, obtaining effectively $M_{\la i,j \ra} = M'_i || C_j$, where the `$||$' operator denotes simultaneous simulation\cite[90]{homer11}. By certain properties of simultaneous simulation and \emph{Linear Speedup Theorem}, we can prove that $M_{\la i,j \ra}$ are $p_j$ time-bounded TMs, which makes them an effective enumeration of polynomial time-bounded TMs. In addition, since $\la i,j \ra \geq j$, we have $M_k$ always run in time $p_k$.
 
Note that the additive term in $p_k$ is necessary since for $k = 1$, by \emph{Linear Speedup Theorem} and its corollaries we can only obtain a time bound $\mathbf{DTIME}(n+1)$ instead of $\mathbf{DTIME}(n)$ for our constructed TMs.

\subsection{Essentials of This Proof}
In the above construction, the desired set is constructed \emph{stage by stage}. At each stage, we will either imitate some elements of the `hard' set $\B$ in $\C$, or set $0$'s, which is the so-called ``holes'' in $\C$. When we are imitating $\B$, we attempt to make the constructed set `hard' enough so that it cannot be reduced to the `easy' problem, thus satisfying certain requirement. When we are ``blowing holes'', we attempt to make the constructed set `easy' enough so that the `hard' problem cannot be reduced to it, thus satisfying certain requirement. The hardness difference of the `easy' problem and the `hard' problem assure that this kind of attempts can always succeed. Therefore by this ``blowing holes'' technique the constructed set can have an immediate hardness.

In the above construction, we will try to find a $x$ to satisfy each requirement. Such value is called a witness for each requirement. In this setting, the above construction is indeed a diagonalization method, Which was first invented by Cantor to prove that real numbers are uncountably many. As each requirement corresponds to each $\P$-time reduction, it is essential for this proof that polynomial time-bounded Turing Machines are effectively enumerable so that we can apply this diagonalization method. However, parameterized reductions cannot be effectively enumerated, hence more generalized diagonalization method is needed, which is the finite injury priority method in the following chapter.
